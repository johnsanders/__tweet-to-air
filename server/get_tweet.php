<?php
	require_once('TwitterAPIExchange.php');
	$settings = file_get_contents('twitter_api_credentials.json');
	$settings = (array)json_decode($settings);

	$twitter = new TwitterAPIExchange($settings);
	function getTweetData($id){
		global $settings, $twitter;
		$url = 'https://api.twitter.com/1.1/statuses/lookup.json';
		$getField = '?id=' . $id . '&tweet_mode=extended';
		$requestMethod = 'GET';

		@$raw = $twitter->setGetfield($getField)
						->buildOauth($url, $requestMethod)
						->performRequest();
		$tweet = json_decode($raw);

		if ( isSet($tweet->errors) && count($tweet->errors) > 0 ){
			$ret = array();
			$ret['status'] = 'error';
			echo json_encode($ret);
			exit;
		}
		if ( count($tweet) == 1 ){
			return $tweet[0];
		} else {
			return $tweet;
		}
	}

	function getRetweeters($id){
		global $settings, $twitter;
		$requestMethod='GET';
		$url = 'https://api.twitter.com/1.1/statuses/retweeters/ids.json';
		$getField = '?stringify_ids=true&id=' . $id;

		@$raw = $twitter->setGetField($getField)
				->buildOauth($url, $requestMethod)
				->performRequest();
		$retweeters = json_decode($raw)->ids;
		if ( count($retweeters >= 11) ){
			$maxRetweets = 11;
		} else {
			$maxRetweets = count($retweeters);
		}
	
		$url = 'https://api.twitter.com/1.1/users/lookup.json';
		$getField = '?user_id=';

		for ($i=0; $i<$maxRetweets-1; $i++){
			if ($i>0){
				$getField .= ',';
			}
			if (isSet($retweeters[$i])){
				$getField .= $retweeters[$i];
			}
		}
		@$raw = $twitter->setGetField($getField)
			->buildOAuth($url, $requestMethod)
			->performRequest();
		$retweeters = json_decode($raw);
		
		$retweetImages = '';
		if ( gettype($retweeters) == 'array') {
			for ($i=0; $i<count($retweeters); $i++){
				if ($i>0){
					$retweetImages .= ',';
				}
					$retweetImages .= $retweeters[$i]->profile_image_url_https;
			}
		}
		return $retweetImages;
	}
?>
