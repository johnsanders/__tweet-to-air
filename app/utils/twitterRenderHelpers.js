var helpers = {
	renderTweet: (tweetId, hiliteStart, hiliteEnd, onRenderFinished) => {
		let url = "http://cnnitouch-prod1/apps/twitter_snapper/renderer_still/snapper.php?id=" + tweetId;
		if (hiliteStart !== null && hiliteEnd !== null) {
			url += "&hiliteStart=" + hiliteStart.toString() + "&hiliteEnd=" + hiliteEnd.toString();
		}
		fetch(url)
		.then(response => response.json())
		.then(json => {
			var tweetTime = new Date(json.date).getTime();
			onRenderFinished(json.filename, tweetTime, json.tweetId, json.twitterUser, json.tweetText, json.tweetText);
		});
	},

	renderAnimation: function(tweetId, twitterUser, tweetTimestamp, userEmail, tweetText, onComplete) {
		let url = "http://cnnitouch-prod2/utilities/bmam/createPrecut.php?user=jsanders&pw=cuse98";
		url += "&tweetId=" + tweetId;
		url += "&twitterUser=" + twitterUser;
		url += "&slug=Tweet @" + twitterUser;
		url += "&date=" + this.createDateString(tweetTimestamp);
		url += "&location=london";

		fetch(url)
			.then(response => response.json())
			.then(msNumber => {

				let url = "/apps/twitter_snapper/render_enqueue.php?id=" + tweetId +
							"&twitterUser=" + twitterUser +
							"&email=" + this.shellEscape(userEmail.replace("'", "")) +
							"&msNumber=" + msNumber +
							"&tweetText=" + tweetText;
				console.log(url);
				fetch(url)
					.then( response => {
						onComplete(msNumber.toString());
					});
			});
	},
	getTweetIdFromUrl: (url) => {
		const regExp = /twitter\.com\/(?:\#!\/)?\w+\/status(es)?\/(\d+)\/?/i;
		return regExp.exec(url)[2];
	},
	createDateString: function(timestamp) {
		var tweetDate = new Date(timestamp);
		var minutes = tweetDate.getUTCMinutes().toString();
		if (minutes.length == 1) {
			minutes = "0" + minutes;
		}
		return tweetDate.getDate().toString() + " " + this.months[tweetDate.getMonth()] +
			" " + tweetDate.getUTCHours() + ":" + minutes;
	},
	shellEscape: (cmd) => {
		return cmd.replace(/(["\s'$`\\])/g,"\\$1");
	},
	months:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]

};

export default helpers;
