//import React from "react";
import ReactDOM from "react-dom";
import routes from "./config/routes";
// eslint-disable no-unused-vars
import Bootstrap from "bootstrap/dist/css/bootstrap.css";
import BootstrapCheckbox from "./css/awesome-bootstrap-checkbox.css";
import IcuCss from "./css/cnnitouch-v2.css";
// eslint-enable no-unused-vars
ReactDOM.render(routes, document.getElementById("app"));
