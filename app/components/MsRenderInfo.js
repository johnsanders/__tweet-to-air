import React from "react";
const MsRenderInfo = () => {
	return(
		<div className="row result">
			<div className="col-xs-12">
				<div className="panel panel-default">
					<div className="panel-heading">
						<div className="panel-title">
							About Mediasource Twitter Animations
						</div>
					</div>
					<div className="panel-body">
						<ul>
							<li>MediaSource animation takes about 5 minutes to render.</li>
							<li>Will create a 45 second MS animation where tweet reveals and floats forward.</li>
							<li>Enter your email so we can tell when it's ready and what the MS number is.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
};
export default MsRenderInfo;
