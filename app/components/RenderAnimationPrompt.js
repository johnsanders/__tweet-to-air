import React from "react";
import LoadingSpinner from "./LoadingSpinner";
import RenderedAnimationInfo from "./RenderedAnimationInfo";
const PropTypes = React.PropTypes;
const RenderAnimationPrompt = (props) => {
	return (
		<div className="row result">
			<div className="col-xs-3">
				<button
					className={"btn btn-primary " + (props.sentToMs ? "disabled" : "")}
					style={{width:"100%"}} onClick={props.renderVideo}>Render Mediasource Animation
				</button>
			</div>
			<div className="col-xs-3">
				<div className={"form-group " + props.emailFormErrorClasses + (props.sentToMs ? "hidden" : "")} >
					<input type="text" id="userEmail" className="form-control" name="email" value={props.userEmail}
						data-toggle="tooltip" data-placement="top" title="Need your Turner email to tell you when it's ready!" placeholder="Your Turner Email"
						ref={(input) => props.setEmailInput(input)}	onChange={props.onUpdateEmail}
					/>
				</div>
			</div>
			<LoadingSpinner containerClass="col-xs-1" visible={props.gettingPrecut} />
			<RenderedAnimationInfo visible={props.sentToMs} msNumber={props.msNumber}/>
		</div>
	);
};
RenderAnimationPrompt.propTypes = {
	sentToMs:PropTypes.bool.isRequired,
	gettingPrecut:PropTypes.bool.isRequired,
	userEmail:PropTypes.string.isRequired,
	emailFormErrorClasses:PropTypes.string.isRequired,
	renderVideo:PropTypes.func.isRequired,
	onUpdateEmail:PropTypes.func.isRequired,
	setEmailInput:PropTypes.func.isRequired,
	msNumber:PropTypes.string
};
export default RenderAnimationPrompt;
