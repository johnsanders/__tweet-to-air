import React from "react";
const PropTypes = React.PropTypes;
const Main = (props) => {
	return (
		<div>
			{props.children}
		</div>
	);
};
Main.propTypes = {
	children:PropTypes.element
};
export default Main;
