import React from "react";
import RenderStatus from "./RenderStatus";
const PropTypes = React.PropTypes;
const RenderStatuses = (props) => {
	return (
		<div className="container-fluid">
			<p className="text-center text-uppercase">{props.title}</p>
			<div className="row">
				<table className={"table table-striped table-bordered " + (props.timedOut ? "inactive" : "")}>
					<colgroup>
						<col span="1" style={{width:"10%"}} />
						<col span="1" style={{width:"20%"}} />
						<col span="1" style={{width:"10%"}} />
						<col span="1" style={{width:"20%"}} />
						<col span="1" style={{width:"10%"}} />
						<col span="1" style={{width:"15%"}} />
						<col span="1" style={{width:"15%"}} />
					</colgroup>
					<thead>
						<tr>
							<td>Type</td>
							<td>Data</td>
							<td>MS Number</td>
							<td>Owner</td>
							<td>Status</td>
							<td>Start</td>
							<td>Finish</td>
						</tr>
					</thead>
					<tbody id="table">
						{props.renders.map(function(render) {
							return (
								<RenderStatus
									key={render.id}
									render={render}
								/>
							);
						})}
					</tbody>
				</table>
			</div>
		</div>
	);
};

RenderStatuses.propTypes = {
	title:PropTypes.string,
	renders:PropTypes.array.isRequired,
	timedOut:PropTypes.bool
};
export default RenderStatuses;
