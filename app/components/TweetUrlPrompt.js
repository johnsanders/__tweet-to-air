import React from "react";
const PropTypes = React.PropTypes;
const TweetUrlPrompt = (props) => {
	return (
		<div>
			<div className="form-group col-xs-12">
				<span className="glyphicon glyphicon-exclamation-sign" style={{color:"red"}}></span>
				<span>&nbsp;Remember, photos in tweets still need to either fit a QuickPass category or be approved for fair use by RACI.</span>
			</div>
			<div className="form-group col-xs-12">
				<input
					className={"form-control"}
					placeholder="Paste the link to your tweet here"
					onChange={props.onUpdateTweetUrl}
					value={props.tweetUrl}
					type="text"
				/>
			</div>
			<div className="form-group">
				<div className="col-xs-12">
					<div className="input checkbox-primary checkbox">
						<input type="checkbox" name="hilite" id="hilite" checked={props.hiliteCheckbox} onChange={props.onUpdateHiliteCheckbox}/>
						<label htmlFor="hilite">Highlight some of the tweet text?</label>
					</div>
				</div>
			</div>
		</div>
	);
};
TweetUrlPrompt.propTypes = {
	tweetUrl:PropTypes.string,
	onUpdateTweetUrl:PropTypes.func.isRequired,
	hiliteCheckbox:PropTypes.bool.isRequired,
	onUpdateHiliteCheckbox:PropTypes.func.isRequired
};
export default TweetUrlPrompt;
