import React from "react";
import Modal from "react-modal";
import instructionsImage from "../img/twitter-guide.jpg";
class Instructions extends React.Component {
	constructor(props) {
		super(props);
		this.state = {visible:false};
		this.toggleVisible = this.toggleVisible.bind(this);
	}
	toggleVisible() {
		this.setState({visible:!this.state.visible});
	}
	render() {
		return (
			<div>
				<div className="form-group col-xs-2">
					<button
						className="btn btn-sm btn-default"
						style={{width:"100%"}}
						data-toggle="modal"
						data-target="#instructions"
						onClick={this.toggleVisible}
					>
						How to use this
					</button>
				</div>
				<Modal
					className="text-center"
					isOpen={this.state.visible}
					contentLabel="How to use"
					style={{content:{marginTop:"70px", outline:"none"}}}
				>
					<div className="modal-dialog modal-lg">
						<div className="modal-content">
							<div className="modal-header">
								<button
									type="button"
									className="close"
									onClick={this.toggleVisible}
								>
									&times;
								</button>
								<h1 className="modal-title">Tweet to Air Instructions</h1>
							</div>
							<div className="modal-body">
								<p>
									Click on the time to get to get to a link that looks something like this:
								</p>
								<p>
									<strong>https://twitter.com/WhiteHouse/status/614515464383918082</strong>
								</p>
								<img src={instructionsImage}/>
							</div>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}
export default Instructions;
