import React from "react";
const PropTypes = React.PropTypes;
class HiliteChooser extends React.Component {
	static propTypes = {
		tweetText:PropTypes.string,
		onUpdateHiliteStartEnd:PropTypes.func.isRequired,
		visible:PropTypes.bool.isRequired
	};

	constructor(props) {
		super(props);
		this.processHilite = this.processHilite.bind(this);
	}
	processHilite() {
		// ONLY THE WINDOW OBJECT KNOWS WHAT'S SELECTED
		const sel = window.getSelection();
		if (!sel.anchorNode) { // IF NOTHING IS SELECTED, JUST RENDER NORMALLY
			return [null, null];
		}
		// IF SOMETHING OTHER THAN THE TWEET TEXT IS SELECTED, DO NOTHING
		if (sel.anchorNode != sel.focusNode || sel.anchorNode.parentNode.id != "hiliteChooserText") {
			return [null, null];
		}

		let hiliteStart = sel.anchorOffset;
		let hiliteEnd = sel.focusOffset;
		if (hiliteStart === undefined || hiliteEnd === undefined) {
			hiliteStart = hiliteEnd = null;
		} else if (hiliteStart == hiliteEnd) {
			hiliteStart = hiliteEnd = null;
		} else if (hiliteStart > hiliteEnd) {
			const tmp = hiliteStart;
			hiliteStart = hiliteEnd;
			hiliteEnd = tmp;
		}
		this.props.onUpdateHiliteStartEnd([hiliteStart, hiliteEnd]);
	}
	render() {
		if (this.props.visible) {
			return (
				<div className="form-group" >
					<div className="col-xs-12">
						<div className="panel panel-primary">
							<div className="panel-heading">
								<div className="panel-title">
									HIGHLIGHT TWEET TEXT
								</div>
							</div>
							<div className="panel-body">
								<div className="row">
									<div className="col-xs-12 text-centered">
										<h3>Select the text you want to highlight, the same way you'd copy/paste</h3>
										<div
											className="well"
											id="hiliteChooserText"
											onMouseUp={this.processHilite}
											dangerouslySetInnerHTML={{__html:this.props.tweetText}}
										>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return null;
		}
	}
}
export default HiliteChooser;
