import React from "react";
import LoadingSpinner from "./LoadingSpinner";
import TweetUrlPrompt from "./TweetUrlPrompt";
import HiliteChooser from "./HiliteChooser";
import Instructions from "./Instructions.js";
import RenderStatusesContainer from "../containers/RenderStatusesContainer.js";
const PropTypes = React.PropTypes;
const Home = (props) => {
	return (
		<div>
			<form onSubmit={props.onSubmitTweetUrl}>
				<Instructions />
				<TweetUrlPrompt
					tweetUrl={props.tweetUrl}
					onUpdateTweetUrl={props.onUpdateTweetUrl}
					hiliteCheckbox={props.hiliteCheckbox}
					onUpdateHiliteCheckbox={props.onUpdateHiliteCheckbox}
					rendering={props.rendering}
					loading={props.loading}
				/>
				<HiliteChooser
					visible={props.hiliteChooserActive}
					onUpdateHiliteStartEnd={props.onUpdateHiliteStartEnd}
					tweetText={props.tweetText}
				/>
				<div className="form-group">
					<div className="col-xs-3">
						<button
							className={"btn btn-block btn-success " + (props.loading || props.rendering ? "disabled" : "")}
							type="submit"
						>
							Get Tweet
						</button>
					</div>
					<LoadingSpinner containerClass="col-xs-1 text-center" visible={props.loading || props.rendering} />
					<div className={"col-xs-8 " + (props.rendering ? "" : "hidden")} style={{paddingTop:"5px", color:"grey"}}>
						<span>Hang on... this'll take 30 seconds or so.</span>
					</div>
				</div>
			</form>
			<div className="form-group">
				<div className="col-xs-12" style={{paddingTop:"20px"}}>
					<RenderStatusesContainer title="PREVIOUS RENDERS" numRenders={15}/>
				</div>
			</div>
		</div>
	);
};
Home.propTypes = {
	tweetUrl:PropTypes.string.isRequired,
	onUpdateTweetUrl:PropTypes.func.isRequired,
	onSubmitTweetUrl:PropTypes.func.isRequired,
	hiliteCheckbox:PropTypes.bool.isRequired,
	onUpdateHiliteCheckbox:PropTypes.func.isRequired,
	onUpdateHiliteStartEnd:PropTypes.func.isRequired,
	loading:PropTypes.bool.isRequired,
	rendering:PropTypes.bool.isRequired,
	hiliteChooserActive:PropTypes.bool,
	tweetText:PropTypes.string
};
export default Home;
