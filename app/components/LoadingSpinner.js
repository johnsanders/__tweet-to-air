import React from "react";
const PropTypes = React.PropTypes;
const LoadingSpinner = (props) => {
	return (props.visible ? (
		<div className={props.containerClass}>
			<i className="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
		</div>
	) : null);
};
LoadingSpinner.propTypes = {
	visible:PropTypes.bool,
	containerClass:PropTypes.string.isRequired
};
export default LoadingSpinner;
