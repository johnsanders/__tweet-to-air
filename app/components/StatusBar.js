import React from "react";
const PropTypes = React.PropTypes;
const StatusBar = (props) => {
	const toPercent = (num) => {
		return (Math.round(parseFloat(num)*100)) + "%";
	};
	return props.render.status.substring(1,2) == "." ? (
		<td id={props.render.id + "_td"}>
			<div className="progress">
				<div
					id={props.render.data1}
					className="progress-bar progress-bar-striped active"
					role="progressbar"
					style={{
						width:toPercent(props.render.status),
						minWidth:"2em"
					}}
					dangerouslySetInnerHTML={{__html:toPercent(props.render.status)}}
				>
				</div>
			</div>
		</td>
	) :
	(
		<td dangerouslySetInnerHTML={{__html:props.render.status}}></td>
	);
};
StatusBar.propTypes = {
	render:PropTypes.object.isRequired
};

export default StatusBar;
