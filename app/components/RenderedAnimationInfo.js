import React from "react";
import {Link} from "react-router";
const PropTypes = React.PropTypes;
const RenderedAnimationInfo = (props) => {
	return (props.visible ? (
		<div className="col-xs-9">
			<span className="glyphicon glyphicon-ok" style={{color:"green"}}></span>&nbsp;
			OK... your MS number will be <strong>{props.msNumber}</strong>.
			We'll email you in about 5 minutes when it's ready.<br />
			You can close this page and watch the render progress <Link to="/renderStatus">here</Link>.
		</div>
	) : null);
};
RenderedAnimationInfo.propTypes = {
	visible:PropTypes.bool.isRequired,
	msNumber:PropTypes.string
};
export default RenderedAnimationInfo;
