import React from "react";
import StatusBar from "./StatusBar";
const PropTypes = React.PropTypes;
const RenderStatus = (props) => {
	const months = ["Jan.", "Feb.", "March", "April", "May", "June", "July", "August", "Sept.", "Oct.", "Nov.", "Dec."];
	const leftPad = (str) => {
		if (str.length < 2) {
			str = "0" + str;
		}
		return str;
	};
	const formatDate = (dateString) => {
		if (dateString) {
			const date = new Date(dateString);
			const hour = leftPad(date.getUTCHours().toString());
			const minute = leftPad(date.getUTCMinutes().toString());
			return date.getUTCDate().toString() + " " + months[date.getUTCMonth()] +
				" | " + hour + ":" + minute + " ET";
		} else {
			return "";
		}
	};
	return (
		<tr>
			<td>{props.render.type.toUpperCase()}</td>
			<td>{props.render.data2}...</td>
			<td>{props.render.msNumber}</td>
			<td>{props.render.email}</td>
			<StatusBar render={props.render} />
			<td>{formatDate(props.render.started, props.render.data1 + "_started")}</td>
			<td>{formatDate(props.render.finished)}</td>
		</tr>
	);
};
RenderStatus.propTypes = {
	render:PropTypes.object.isRequired
};
export default RenderStatus;
