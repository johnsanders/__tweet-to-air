import React from "react";
import {Link} from "react-router";
import LoadingSpinner from "./LoadingSpinner";
import RenderAnimationPromptContainer from "../containers/RenderAnimationPromptContainer";
import MsRenderInfo from "./MsRenderInfo";
import RawLinks from "./RawLinks";
const PropTypes = React.PropTypes;
const ViewRender = (props) => {
	return (
		<div>
			<div className="row result">
				<div className="col-xs-12">
					<div className="panel panel-primary">
						<div className="panel-heading">
							<div className="panel-title">
								Tweet Still Image Rendered
							</div>
						</div>
						<div className="panel-body">
							<div className="col-xs-8">
								<ul>
									<li>This is now available as a cnnimage on the cnnitouch server under the name:
										<h4><span id="filename">{props.filename}</span></h4>
									</li>
									<li>Use the buttons below to send the tweet to a Viz Pilot, a MediaSource animation, download the file or show fullscreen.</li>
								</ul>
							</div>
							<div className="col-xs-4">
								<img
									className="img-responsive" 
									height="200" 
									src={props.imagePath + props.filename}
									ref={ (component) => props.setImageRef(component) }/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="row result">
				<div className="col-xs-3">
					<button 
						className={"form-control btn btn-primary " + (props.sendingToViz || props.sentToViz ? "disabled" : "")}
						type="submit"
						value="Submit"
						onClick={props.sendToViz}
					>
						Send This Image to Viz
					</button>
				</div>
				<LoadingSpinner containerClass="col-xs-9" visible={props.sendingToViz} />
				<div className={"col-sx-9 " + (props.sentToViz ? "" : "hidden")} id="vizSent">
				<span className="glyphicon glyphicon-ok" style={{color:"green"}}></span> On its way! Should show up in Viz within 5 minutes.
				</div>
			</div>
			<div className="row result">
				<div className="col-xs-3">
					<a download={props.filename} href={"/cnnimages/" + props.filename} className="btn btn-primary" style={{width:"100%"}}>Download Image</a>
				</div>
			</div>
			<div className="row result">
				<div className="col-xs-3">
					<button className="btn btn-primary" style={{width:"100%"}} onClick={props.showImageFull}>
						Show Fullscreen
					</button>
				</div>
			</div>
			<RenderAnimationPromptContainer
				tweetId={props.tweetId}
				twitterUser={props.twitterUser}
				tweetTimestamp={props.tweetTimestamp}
				tweetText={props.tweetText}
			/>
			<MsRenderInfo />
			<RawLinks tweetId={props.tweetId} />
		</div>
	);
};
ViewRender.propTypes = {
	filename:PropTypes.string.isRequired,
	imagePath:PropTypes.string.isRequired,
	sendingToViz:PropTypes.bool.isRequired,
	sentToViz:PropTypes.bool.isRequired,
	sendToViz:PropTypes.func.isRequired,
	tweetId:PropTypes.string.isRequired,
	twitterUser:PropTypes.string.isRequired,
	tweetTimestamp:PropTypes.number.isRequired
};
ViewRender.contextTypes = {
	router: React.PropTypes.object.isRequired
};
export default ViewRender;
