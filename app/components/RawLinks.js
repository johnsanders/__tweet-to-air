import React from "react";
const style= {color:"#F0F0F0", marginRight:"20px"};

const RawLinks = props => (
	<div>
		<a
			style={style}
			href={`/cnnimages/twitter_snapper_${props.tweetId}.png`}
		>
			Raw Tweet
		</a>
		<a
			style={style}
			href={`/cnnimages/twitter_snapper_${props.tweetId}_profile.png`}
		>
			Raw Profile
		</a>
	</div>
);

export default RawLinks;
