<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Tweet to Air</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<style>
			body {
				display:none;
			}
			.row {
				margin-top:10px;
			}
			.progress {
				margin-bottom:inherit;
			}
			table.inactive {
				opacity:0.3;
			}
		</style>
	</head'>
	<body>
		<?php require('/home/cnnitouch/www/adminui/nav.php'); ?>
		<div class="container">
			<div class="page-header text-center">
				<h2>TWEET TO AIR</h2>
			</div>
		</div>
		<div id="app" class="container-fluid"></div>
	</body>
</html>
