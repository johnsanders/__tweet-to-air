import React from "react";
import RenderAnimationPrompt from "../components/RenderAnimationPrompt.js";
import twitterRenderHelpers from "../utils/twitterRenderHelpers.js";
const PropTypes = React.PropTypes;
class RenderAnimationPromptContainer extends React.Component {
	static propTypes = {
		tweetId:PropTypes.string.isRequired,
		twitterUser:PropTypes.string.isRequired,
		userEmail:PropTypes.string,
		tweetTimestamp:PropTypes.number.isRequired
	};
	constructor(props) {
		super(props);
		this.state = {
			gettingPrecut: false,
			sentToMs: false,
			userEmail: "",
			emailFormErrorClasses: ""
		};
		this.renderVideo = this.renderVideo.bind(this);
		this.onUpdateEmail = this.onUpdateEmail.bind(this);
		this.onRenderComplete = this.onRenderComplete.bind(this);
		this.setEmailInput = this.setEmailInput.bind(this);
	}
	renderVideo(e) {
		e.preventDefault();
		if (this.sentToMs) {
			return;
		}
		if (!this.emailIsValid(this.state.userEmail)) {
			this.setState({emailFormErrorClasses:"has-error has-feedback"});
			this.emailInput.focus();
			return;
		}
		this.setState({gettingPrecut: true});
		twitterRenderHelpers.renderAnimation(this.props.tweetId, this.props.twitterUser, this.props.tweetTimestamp, this.state.userEmail, this.props.tweetText, this.onRenderComplete);
	}
	emailIsValid(userEmail) {
		return userEmail.slice(-11) == "@turner.com" || (userEmail.slice(-8) == "@cnn.com");
	}
	onRenderComplete(msNumber) {
		this.setState({msNumber:msNumber, sentToMs:true, gettingPrecut:false});
	}
	onUpdateEmail(e) {
		this.setState({userEmail:e.target.value});
	}
	setEmailInput(input) {
		this.emailInput = input;
	}
	render() {
		return (
			<RenderAnimationPrompt
				emailFormErrorClasses = {this.state.emailFormErrorClasses}
				onUpdateEmail = {this.onUpdateEmail}
				renderVideo = {this.renderVideo}
				sentToMs = {this.state.sentToMs}
				userEmail = {this.state.userEmail}
				gettingPrecut = {this.state.gettingPrecut}
				msNumber = {this.state.msNumber}
				setEmailInput = {this.setEmailInput}
			/>
		);
	}
}
export default RenderAnimationPromptContainer;
