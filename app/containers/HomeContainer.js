import React from "react";
import Home from "../components/Home";
import twitterRenderHelpers from "../utils/twitterRenderHelpers";
class HomeContainer extends React.Component {
	static contextTypes = {
		router: React.PropTypes.object.isRequired
	};
	constructor(props) {
		super(props);
		this.state = {
			tweetUrl:"",
			hiliteCheckbox:false,
			loading:false,
			rendering:false,
			hiliteChooserActive:false
		};
		this.hiliteStartEnd = [null, null];
		this.onUpdateTweetUrl = this.onUpdateTweetUrl.bind(this);
		this.onSubmitTweetUrl = this.onSubmitTweetUrl.bind(this);
		this.onUpdateHiliteCheckbox = this.onUpdateHiliteCheckbox.bind(this);
		this.onUpdateHiliteStartEnd = this.onUpdateHiliteStartEnd.bind(this);
		this.onRenderFinished = this.onRenderFinished.bind(this);
	}
	componentDidMount(){
		// ELSE WE GET A FLASH OF UNSTYLED CRAP
		document.getElementsByTagName("body")[0].style.display = "inherit";
	}
	onUpdateTweetUrl(e) {
		if (this.state.loading || this.state.rendering) {
			return;
		}
		this.setState({tweetUrl:e.target.value});
	}
	onUpdateHiliteCheckbox() {
		if (this.state.rendering) {
			return;
		} else if (this.state.hiliteChooserActive) {
			this.setState({hiliteChooserActive:false});
			this.hiliteStartEnd = [null, null];
			// ONLY WINDOW OBJECT CAN SEE OR CLEAR SELECTED TEXT
			window.getSelection().removeAllRanges();
		}
		this.setState({hiliteCheckbox:!this.state.hiliteCheckbox});
	}
	onSubmitTweetUrl(e) {
		e.preventDefault();
		this.tweetId = twitterRenderHelpers.getTweetIdFromUrl(this.state.tweetUrl);
		if (this.state.tweetUrl == "") {
			return;
		}
		if (this.state.hiliteCheckbox && !this.state.hiliteChooserActive) {
			this.displayHiliteChooser();
		} else if (this.state.hiliteChooserActive && this.hiliteStartEnd[0] !== null) {
			this.setState({rendering:true});
			twitterRenderHelpers.renderTweet(this.tweetId, this.hiliteStartEnd[0], this.hiliteStartEnd[1], this.onRenderFinished);
		} else {
			this.setState({rendering:true});
			twitterRenderHelpers.renderTweet(this.tweetId, null, null, this.onRenderFinished);
		}
	}
	displayHiliteChooser() {
		this.setState({loading:true});
		fetch("/apps/twitter_snapper/get_tweet_data.php?id=" + this.tweetId)
				.then(response=>response.json())
				.then(tweet => {
					this.setState({
						hiliteChooserActive: true,
						loading:false,
						tweetText:tweet.full_text
					});
				});
	}
	onUpdateHiliteStartEnd(hiliteStartEnd) {
		this.hiliteStartEnd = hiliteStartEnd;
	}
	onRenderFinished(filename, tweetTime, tweetId, twitterUser, tweetText) {
		this.setState({url:"", loading:false, rendering:false});
		this.context.router.push({
			pathname:"/viewRender",
			query: {
				tweetId:tweetId,
				tweetDate:tweetTime,
				twitterUser:twitterUser,
				filename:filename,
				tweetText:encodeURIComponent(tweetText.substring(0,30))
			}
		});
	}
	render() {
		return (
			<Home
				tweetUrl={this.state.tweetUrl}
				tweetText={this.state.tweetText}
				onUpdateTweetUrl={this.onUpdateTweetUrl}
				onSubmitTweetUrl={this.onSubmitTweetUrl}
				hiliteChooserActive={this.state.hiliteChooserActive}
				hiliteCheckbox={this.state.hiliteCheckbox}
				onUpdateHiliteCheckbox={this.onUpdateHiliteCheckbox}
				onUpdateHiliteStartEnd={this.onUpdateHiliteStartEnd}
				rendering={this.state.rendering}
				loading={this.state.loading}
			/>
		);
	}
}
export default HomeContainer;
