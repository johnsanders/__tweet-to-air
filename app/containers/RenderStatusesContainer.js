import React from "react";
import RenderStatuses from "../components/RenderStatuses";
const PropTypes = React.PropTypes;
class RenderStatusesContainer extends React.Component {
	static defaultProps = {
		title:"RENDER STATUS",
		numRenders:50
	};
	static propTypes = {
		title:PropTypes.string,
		numRenders:PropTypes.number
	};

	constructor(props) {
		super(props);
		this.state = {renders:[], timedOut:false};
		this.updateCount = 0;
		this.dataUrl = "/apps/flashSQLinterface/read3.php?table=status_render&" +
						"where=started%20>%20NOW()%20-%20INTERVAL%2010%20DAY%20OR%20started%20IS%20NULL";
	}
	componentDidMount() {
		document.getElementsByTagName("body")[0].style.display = "inherit";
		this.updateStatus();
		this.interval = setInterval(this.updateStatus.bind(this), 3000);
	}
	componentWillUnmount() {
		clearInterval(this.interval);
	}
	updateStatus() {
		this.updateCount++;
		if (this.updateCount > 100) {
			clearInterval(this.interval);
			this.setState({timedOut:true});
		}
		fetch(this.dataUrl)
			.then (response => response.json())
			.then (response => {
				this.setState({renders:response.reverse().slice(0, this.props.numRenders)});
			});
	}
	render() {
		return (
			<RenderStatuses
				title={this.props.title}
				renders={this.state.renders}
				timedOut={this.state.timedOut}
			/>
		);
	}
}
export default RenderStatusesContainer;
