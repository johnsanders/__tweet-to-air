import React from "react";
import ViewRender from "../components/ViewRender";
const PropTypes = React.PropTypes;
class ViewRenderContainer extends React.Component {
	static propTypes = {
		location:PropTypes.object.isRequired
	};
	constructor(props) {
		super(props);
		this.state = {
			sendingToViz: false,
			sentToViz: false,
			filename:this.props.location.query.filename,
			twitterUser:this.props.location.query.twitterUser,
			tweetId:this.props.location.query.tweetId,
			tweetText:this.props.location.query.tweetText,
			tweetTimestamp:parseInt(this.props.location.query.tweetDate)
		};
		this.imagePath = "/cnnimages/";
		this.sendToViz = this.sendToViz.bind(this);
		this.setImageRef = this.setImageRef.bind(this);
		this.showImageFull = this.showImageFull.bind(this);
	}
	componentDidMount(){
		// ELSE WE GET A FLASH OF UNSTYLED CRAP
		document.getElementsByTagName("body")[0].style.display = "inherit";
	}
	setImageRef(component){
		this.imageComponent = component;
	}
	showImageFull(){
		if (this.imageComponent.requestFullscreen) {
			this.imageComponent.requestFullscreen();
		} else if (this.imageComponent.webkitRequestFullscreen) {
			this.imageComponent.webkitRequestFullscreen();
		}
	}
	sendToViz(e) {
		e.preventDefault();
		if (this.state.sentToViz) {
			return;
		}
		this.setState({sendingToViz:true});
		fetch("/utilities/newsimages_copy.php?path=/home/cnnitouch/www/cnnimages/&file=" + this.state.filename)
			.then(() => {
				this.setState({sendingToViz:false, sentToViz:true});
			});
	}

	render() {
		return (
			<ViewRender
				imagePath={this.imagePath}
				filename={this.state.filename}
				tweetId={this.state.tweetId}
				tweetText={this.state.tweetText}
				twitterUser={this.state.twitterUser}
				tweetTimestamp={this.state.tweetTimestamp}
				setImageRef={this.setImageRef}
				showImageFull={this.showImageFull}
				sendingToViz={this.state.sendingToViz}
				sentToViz={this.state.sentToViz}
				sendToViz={this.sendToViz}
			/>
		);
	}
}
export default ViewRenderContainer;
