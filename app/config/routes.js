import React from "react";
import {Router, Route, hashHistory, IndexRoute} from "react-router";
import Main from "../components/Main";
import HomeContainer from "../containers/HomeContainer";
import ViewRenderContainer from "../containers/ViewRenderContainer";
import RenderStatusesContainer from "../containers/RenderStatusesContainer";
const routes = (
	<Router history={hashHistory}>
		<Route path="/" component={Main}>
			<IndexRoute component={HomeContainer} />
			<Route path="viewRender" component={ViewRenderContainer} />
			<Route path="renderStatus" component={RenderStatusesContainer} />
		</Route>
	</Router>
);
export default routes;
